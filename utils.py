def delim_reader(filepath, delim, order_spec):
    lines = open(filepath, 'r').readlines()
    return [construct(line.split(delim), order_spec) for line in lines]

def construct(delimited, order_spec):
    print(delimited)
    print(order_spec)
    return {
        k: delimited[v].strip() if v<len(delimited) else "" for k,v in order_spec.items()
    }
