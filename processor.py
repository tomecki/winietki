import cairosvg
import logging
import os
import progressbar

from io import StringIO, BytesIO
from utils import delim_reader
from jinja2 import Template

log = logging.getLogger(__name__)

def process(
    name_file_path,
    template_file_path,
    output_path,
    order_spec,
    config,
):
    user_input = delim_reader(
        filepath=name_file_path,
        delim=config['arguments'].get('delimiter') or ' ',
        order_spec=order_spec,
    )

    template_file = "".join(open(template_file_path).readlines())

    template = Template(template_file)
    with progressbar.ProgressBar(max_value=len(user_input)) as bar:
        for i, entry in enumerate(user_input):
            try:
                result = template.render(entry)
                result_path = os.path.join(output_path, config['result'].get('filename').format(**entry))
                {
                    'svg': save_svg(result, result_path),
                    'pdf': save_pdf(result, result_path),
                }.get(config['arguments'].get('filetype'))

            except Exception as e:
                log.error("Error processing entry: {}\nError message: {}".format(entry, e))
            bar.update(i)

def save_svg(content, output_path):
    result_file = open("{}.svg".format(output_path), 'w')
    result_file.write(content)
    result_file.close()

def save_pdf(content, output_path):
    save_svg(content, output_path)
    cairosvg.svg2pdf(url='{}.svg'.format(output_path), write_to='{}.pdf'.format(output_path))
    os.remove("{}.svg".format(output_path))
