#!/usr/bin/env python3

import argparse
import logging
import processor
from configparser import ConfigParser

log = logging.getLogger(__name__)

def main():
    parser = argparse.ArgumentParser(description='Templated document generator')
    parser.add_argument('--config', nargs='?', help='Configuration file', default="config.cfg")
    parser.add_argument('--data-file', nargs='?', help='File containing data entries in lines', )
    parser.add_argument('--template-file', nargs='?', help='Template file')
    parser.add_argument('--output-path', nargs='?', help='Output path')
    args = parser.parse_args()

    config = ConfigParser()
    try:
        config.read_file(open(args.config))
    except FileNotFoundError:
        log.error("You must supply configuration file!")
        parser.print_help()
        return

    name_file_path = args.data_file or config['arguments'].get('name_file')
    template_file_path = args.template_file or config['arguments'].get('template_file')
    output_path = args.output_path or config['arguments'].get('output_path')

    if not all ([name_file_path, template_file_path, output_path]):
        parser.print_help()
        return

    order_spec = {k: config['template'].getint(k) for k in config['template'].keys()}

    processor.process(
        name_file_path,
        template_file_path,
        output_path,
        order_spec,
        config)




if __name__=="__main__":
    main()
